import logging
import datetime
import aiohttp
import math

import config

emoji = {
    "suburban": "<:SBahn:1102206882527060038>",
    "regional": "<:desiro:1103976094408921128>",
    "express": "<:ICELogo:1102210303518846976>",
    "bus": "<:bus:1103708033537802271>",
    "ferry": ":ferry:",
    "subway": "<:subway:1103708020111847424>",
    "tram": "<:tram:1103708003552731317>",
    "taxi": ":taxi:",
}

color = {
    "suburban": "36175",
    "regional": "24483",
    "express": "16712708",
    "bus": "10682492",
    "tram": "14230058"
}

class WebhookConnector():
    def __init__(self, config: config.Config):
        self.url = config.get_key("webhook_url")

    async def post_to_webhook(self, event, stopovers):
        def timestamp(iso):
            # returns iso 8601 → unix timestamp
            return int(datetime.datetime.fromisoformat(iso).timestamp())

        def minutes_delay(t1, t2):
            # returns the delay in rounded up minutes
            t1 = timestamp(t1)
            t2 = timestamp(t2)
            t = t2-t1
            return "" if t <= 60 else " (+"+str(math.ceil(t/60))+")"

        webhook_url = self.url

        webhook_json = """{{
            "content": "{body}",
            "embeds": [
                {{
                {optional}
                "title": "{emoji} **[{train_line}]** {line_destination}",
                "description": "Ankunft <t:{arrival_timestamp}:R> | {stations}",
                "url": "{train_url}",
                "color": {color},
                "fields": [
                    {{
                    "name": "{start_station}",
                    "value": "<t:{start_timestamp}:t>{start_delay}",
                    "inline": true
                    }},
                    {{
                    "name": "{end_station}",
                    "value": "<t:{end_timestamp}:t>{end_delay}",
                    "inline": true
                    }}
                ],
                "author": {{
                    "name": "{username} ist unterwegs",
                    "icon_url": "{profile_picture_url}"
                }},
                "footer": {{
                    "text": "⏲️ {trip_length} 🗺️ {distance} 🏃‍♀️ {speed}"
                }}
                }}
            ],
            "attachments": []
        }}"""

        if event["train"]["origin"]["departureReal"] is None:
            event["train"]["origin"]["departureReal"] = event["train"]["origin"]["departure"]
        
        if event["train"]["destination"]["arrivalReal"] is None:
            event["train"]["destination"]["arrivalReal"] = event["train"]["destination"]["arrival"]

        line_destination = "→ " + stopovers[-1]["name"]

        origin_name = event["train"]["origin"]["name"]
        dest_name = event["train"]["destination"]["name"]

        origin_index = next((i for i in range(len(stopovers)) if stopovers[i]["name"] == origin_name), None)
        dest_index = next((i for i in range(len(stopovers)) if stopovers[i]["name"] == dest_name), None)

        if origin_index is None or dest_index is None or dest_index - origin_index <= 0:
            logging.warn("looking for origin_name" + origin_name + " and dest_name " + dest_name + " - " + str(origin_index) + str(dest_index))
            stations = ""
            line_destination = ""
        else:
            stations = str(dest_index - origin_index) + " Stationen"


        optional = ""

        if event["train"]["lineName"] == "STR 4" and event["username"] == "uni":
            optional = """"image": {
                "url": "https://cdn.discordapp.com/attachments/501109105117757460/1106254505353416754/image.png"
            },"""

        if "Mühlburger Tor" in event["train"]["origin"]["name"] or "Mühlburger Tor" in event["train"]["destination"]["name"]:
            optional = """"image": {
                "url": "https://cdn.discordapp.com/attachments/1102270734782439526/1106267362199613461/FOW81hQWQAAqyrB.jpg"
            },"""

        if "Durlacher Tor" in event["train"]["origin"]["name"] or "Durlacher Tor" in event["train"]["destination"]["name"]:
            optional = """"image": {
                "url": "https://cdn.discordapp.com/attachments/1102270734782439526/1117534093794627636/cooltext436604545606909.gif"
            },"""

        webhook_json = webhook_json.format(
            username=event["username"],
            profile_picture_url=event["profilePicture"],
            train_url="https://traewelling.de/status/"+str(event["id"]),
            emoji=emoji[event["train"]["category"]] if event["train"]["category"] in emoji else ":train:",
            color=color[event["train"]["category"]] if event["train"]["category"] in color else "43520",
            train_line=event["train"]["lineName"],
            stations=stations,
            line_destination=line_destination,
            arrival_timestamp=timestamp(event["train"]["destination"]["arrivalReal"]),
            start_station=event["train"]["origin"]["name"],
            start_timestamp=timestamp(event["train"]["origin"]["departureReal"]),
            start_delay=minutes_delay(event["train"]["origin"]["departurePlanned"], event["train"]["origin"]["departureReal"]),
            end_station=event["train"]["destination"]["name"],
            end_timestamp=timestamp(event["train"]["destination"]["arrivalReal"]),
            end_delay=minutes_delay(event["train"]["destination"]["arrivalPlanned"], event["train"]["destination"]["arrivalReal"]),
            trip_length=str(event["train"]["duration"]) + " min",
            distance=str(round(event["train"]["distance"]/1000, 1)) + " km",
            speed=str(int(round(event["train"]["speed"], 0))) + " km/h",
            body="*\\\""+event["body"].replace("*", "").replace("\"", "")+"\\\"*" if len(event["body"]) > 0 else "",
            optional=optional
        )
        
        logging.debug("replace")

        webhook_json = webhook_json.replace("{{", "{").replace("}}", "}")

        logging.debug("json:")
        logging.debug(webhook_json)
        logging.debug(len(webhook_json))

        async with aiohttp.ClientSession(headers={
            "Content-Type": "application/json"
        }) as sess:
            async with sess.post(webhook_url, data=webhook_json) as response:
                if response.status == 200 or response.status == 204:
                    logging.info("Webhook answered with code " + str(response.status))
                    logging.debug(await response.text())
                else:
                    logging.error("Webhook answered with code " + str(response.status))
                    text = await response.text()
                    logging.error(text)
                    raise Exception("Webhook answered with code " + str(response.status) + " " + text)
