import asyncio
import logging
import os

import config as configM
import db as dbM
import webhook_connector as webhook_connectorM
import discord_bot as discord_botM
import traewelling as traewellingM

async def test():
    # data = await traewelling.get_status_for_user("uni")
    import json
    with open("example-data.json", "r") as r:
        data = json.load(r)
    single = data["data"][0]
    await webhook_connector.post_to_webhook(single)

if __name__ == "__main__":
    CONFIG_LOCATION = os.path.dirname(__file__) + "/config.json"
    DATABASE_LOCATION = os.path.dirname(__file__) + "/database.sqlite3"
    logging.basicConfig(level=logging.INFO)

    config = configM.Config()
    config.read_config(CONFIG_LOCATION)

    db = dbM.DB(DATABASE_LOCATION)

    webhook_connector = webhook_connectorM.WebhookConnector(config)
    
    discord_bot = discord_botM.DiscordBot(config, db)

    traewelling = traewellingM.Traewelling(config, db, webhook_connector)

    loop = asyncio.get_event_loop()
    loop.create_task(discord_bot.bot.start(config.get_key("discord_token")))
    loop.run_until_complete(traewelling.refresh_loop())

    db.close()